# advanced-propaties.styl
「advanced-propaties.styl」は既存のCSSプロパティを拡張し、
より直感的でシンプルにスタイルシートを記述できるように書かれたstylusライブラリです。
例えば、こんな風にスタイルを記述できるようになります。

```
.BlackCircle{
  circle: 100px; //advanced-propatiesによる新プロパティ
  background: #000;
}
``` 

# Dependency
Stylus

# Setup
stylusファイルにadvanced-propaties.stylをimportしたらセットアップ完了です。

# Usage

advanced-propatiesに定義されたプロパティを指定して使います。

ex )黒い円を作りたいとき
```
.BlackCircle{
  circle: 100px; //advanced-propatiesによる新プロパティ
  background: #000;
}
``` 

現在、存在しているプロパティは以下になります。

- rect: [width] [height];
- round-rect: [width] [height] [border-radius];
- square: [size];
- round-square: [size] [border-radius];
- circle: [size];

# Authors
YutoSeta
Twitter: @YutoSeta
